from .architectures.autoencoders import *


def get_network(architecture, input_dims=None, output_dims=None, input_params=None, name=None):
    if architecture == 'KerasConvAutoEncoder':
        return KerasConvAutoEncoder(
            input_dims=input_dims,
            output_dims=output_dims,
            input_params=input_params,
            name=name)
    elif architecture == 'KerasMultiOutConvAutoEncoder':
        return KerasMultiOutConvAutoEncoder(
            input_dims=input_dims,
            output_dims=output_dims,
            input_params=input_params,
            name=name)
    elif architecture == 'KerasMLPAutoEncoder':
        return KerasMLPAutoEncoder(
            input_dims=input_dims,
            output_dims=output_dims,
            input_params=input_params,
            name=name)
