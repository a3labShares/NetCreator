# import theano.tensor as T
# import tensorflow as T

def get_crop_dimension(input_shape, desired_shape):
    import math
    deltas = tuple(float(x - y) for x, y in zip(input_shape, desired_shape))
    crop = tuple((int(math.ceil(delta / 2)), int(math.floor(delta / 2))) for delta in deltas)
    if len(crop) == 1:
        crop = crop[0]

    return crop


# def custom_relu_range_neg1_pos1(x, alpha=0.1, max_value=None, min_value=-1.0):
#     x = T.nnet.relu(x, alpha)
#     if max_value is not None:
#         x = T.minimum(x, max_value)
#     if min_value is not None:
#         x = T.maximum(x, min_value)
#     return x
