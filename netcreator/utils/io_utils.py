import os
import json


def dimension_check(dims):
    """" Check that the dimension is a list and extract dimensions if arrays with features/labels are given """

    if not isinstance(dims, dict):
        output_dims = {}
        if isinstance(dims, tuple):
            output_dims.update({'1': list(dims)})

        elif isinstance(dims, list):
            for i, v in enumerate(dims):
                if isinstance(v, tuple):
                    output_dims.update({str(i): list(dims)})
                elif isinstance(v, list):
                    output_dims.update({str(i): dims})

    else:
        # TODO check the dictionary is correct
        output_dims = dims

    return output_dims


def save_json(path, filename, dictionary, overwrite=True):
    try:
        if not isinstance(dictionary, dict):
            raise ValueError('{} is not a dictionary.'.format(dictionary))

        complete_filename = os.path.join(path, filename)
        if not os.path.isfile(filename):
            with open(complete_filename, 'w') as output_file:
                json.dump(dictionary, output_file, indent=4, sort_keys=True)
        else:
            if overwrite:
                with open(complete_filename, 'w') as output_file:
                    json.dump(dictionary, output_file, indent=4, sort_keys=True)
                print('Warning: {} was overwritten.'.format(filename))
            else:
                print('Warning: {} was NOT overwritten.'.format(filename))

    except (TypeError, ValueError) as err:
        return "Error: {}".format(err)


def load_json(path, filename):
    try:
        spec_filename = os.path.join(path, filename)
        with open(spec_filename) as data_file:
            return json.load(data_file)

    except (TypeError, ValueError) as err:
        return "Error: {}".format(err)
