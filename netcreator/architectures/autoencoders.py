from __future__ import absolute_import

from ..base.convolutionals import _KerasNet, _ConvNet, _KerasConvNet, _Net


class _ConvAutoEncoder(_ConvNet):
    def get_default_parameters(self):

        net_params = {
            'input_gaussian_noise': 0.,
            'conv_dimensions': [8],
            'enc_activation': 'linear',
            'dec_activation': 'relu',
            'conv_windows': 5,
            'conv_strides': 1,
            'conv_dropouts': 0.,
            'pooling_type': 'max',
            'pooling_windows': 5,
            'pooling_strides': None,
            'dense_dimensions': [128],
            'dense_activation': 'relu',
            'dense_dropouts': 0.,
            'batch_normalization': False,
            'output_activation': 'linear',
            'clipping_threshold': 'none'
        }

        return net_params

    def check_parameters(self, net_params):
        n_conv = len(net_params['conv_dimensions'])
        n_dense = len(net_params['dense_dimensions'])

        if n_conv < 1:
            raise ValueError('You need at least one convolutional layer.')

        if isinstance(net_params['conv_dropouts'], float):
            net_params['conv_dropouts'] = [net_params['conv_dropouts']]
            for i in range(n_conv - 1):
                net_params['conv_dropouts'].append(net_params['conv_dropouts'][0])
        elif isinstance(net_params['conv_dropouts'], list):
            if n_conv != len(net_params['conv_dropouts']):
                raise ValueError('Number of convolutional layers and dropouts not matching.')
        else:
            raise ValueError('Convolutional dropouts type not supported.')

        if isinstance(net_params['conv_windows'], int):
            net_params['conv_windows'] = [net_params['conv_windows']]
            for i in range(n_conv - 1):
                net_params['conv_windows'].append(net_params['conv_windows'][0])
        elif isinstance(net_params['conv_windows'], list):
            if n_conv != len(net_params['conv_windows']):
                raise ValueError('Number of convolutional layers and windows not matching.')
        else:
            raise ValueError('Convolutional windows type not supported.')

        if isinstance(net_params['conv_strides'], int):
            net_params['conv_strides'] = [net_params['conv_strides']]
            for i in range(n_conv - 1):
                net_params['conv_strides'].append(net_params['conv_strides'][0])
        elif isinstance(net_params['conv_strides'], list):
            if n_conv != len(net_params['conv_strides']):
                raise ValueError('Number of convolutional layers and strides not matching.')
        else:
            raise ValueError('Convolutional strides type not supported.')

        if isinstance(net_params['pooling_windows'], int):
            net_params['pooling_windows'] = [net_params['pooling_windows']]
            for i in range(n_conv - 1):
                net_params['pooling_windows'].append(net_params['pooling_windows'][0])
        elif isinstance(net_params['pooling_windows'], list):
            if n_conv != len(net_params['pooling_windows']):
                raise ValueError('Number of convolutional layers and pooling windows not matching.')
        else:
            raise ValueError('Convolutional windows type not supported.')

        if net_params['pooling_strides'] is None:
            net_params['pooling_strides'] = [None]
            for i in range(n_conv - 1):
                net_params['pooling_strides'].append(None)
        elif isinstance(net_params['pooling_strides'], int):
            net_params['pooling_strides'] = [net_params['pooling_strides']]
            for i in range(n_conv - 1):
                net_params['pooling_strides'].append(net_params['pooling_strides'][0])
        elif isinstance(net_params['pooling_strides'], list):
            if n_conv != len(net_params['pooling_strides']):
                raise ValueError('Number of convolutional layers and pooling strides not matching.')
        else:
            raise ValueError('Convolutional windows type not supported.')

        if isinstance(net_params['dense_dropouts'], float):
            net_params['dense_dropouts'] = [net_params['dense_dropouts']]
            for i in range(n_dense - 1):
                net_params['dense_dropouts'].append(net_params['dense_dropouts'][0])
        elif isinstance(net_params['dense_dropouts'], list):
            if n_dense != len(net_params['dense_dropouts']):
                raise ValueError('Number of dense layers and dropouts not matching.')
        else:
            raise ValueError('Dense dropouts type not supported.')

        if net_params['batch_normalization']:
            if not all(conv_dropout == 0. for conv_dropout in net_params['conv_dropouts']):
                raise ValueError('Use either dropout or batch normalization.')
            elif not all(dense_dropout == 0. for dense_dropout in net_params['dense_dropouts']):
                raise ValueError('Use either dropout or batch normalization.')

        if net_params['output_activation'] == 'clipped_relu':
            if not isinstance(net_params['clipping_threshold'], float):
                raise ValueError('Clipped relu detected for output, use a float number as clipping threshold')
        else:
            net_params['clipping_threshold'] = 'none'


class _MLPAutoEncoder(_Net):
    def get_default_parameters(self):

        net_params = {
            'input_gaussian_noise': 0.,
            'enc_activation': 'linear',
            'dec_activation': 'relu',
            'dense_dimensions': [128],
            'dense_dropouts': 0.,
            'batch_normalization': False,
            'output_activation': 'linear',
            'clipping_threshold': 'none'
        }

        return net_params

    def check_parameters(self, net_params):
        return


class KerasConvAutoEncoder(_ConvAutoEncoder, _KerasConvNet):
    def build_model(self):
        from keras.models import Model
        from keras.layers import Flatten, Reshape, Dropout, Dense, BatchNormalization, GaussianNoise, ZeroPadding1D
        from ..utils.net_utils import get_crop_dimension

        pooling_input_dimensions = []

        input_layers = self.get_input_layers()
        input_layer = input_layers[next(iter(input_layers))]

        layers = self.get_conv_layers()

        # Network instantiation
        x = input_layer

        if self.specs['net_params']['input_gaussian_noise'] != 0.:
            x = GaussianNoise(stddev=self.specs['net_params']['input_gaussian_noise'],
                              name=self.specs['name'] + '_GaussianNoise')(x)
        # Encoding convolutional layers
        for layer_index in range(len(self.specs['net_params']['conv_dimensions'])):

            if layer_index > 0 and self.specs['net_params']['batch_normalization']:
                x = BatchNormalization(name=self.specs['name'] + '_EncConvBatchNorm' + str(layer_index + 1))(x)
            x = layers['conv_layer'](filters=self.specs['net_params']['conv_dimensions'][layer_index],
                                     kernel_size=self.specs['net_params']['conv_windows'][layer_index],
                                     strides=self.specs['net_params']['conv_strides'][layer_index],
                                     padding='valid',
                                     activation=self.specs['net_params']['enc_activation'],
                                     name=self.specs['name'] + '_EncConv' + str(layer_index + 1))(x)

            pooling_input_dimensions.append(x._keras_shape[1:-1])

            if layers['pool_layer'] is not None and self.specs['net_params']['pooling_windows'][layer_index] != 1:  # exectu if pooling window > 1
                x = layers['pool_layer'](pool_size=self.specs['net_params']['pooling_windows'][layer_index],
                                         strides=self.specs['net_params']['pooling_strides'][layer_index],
                                         padding='same',
                                         name=self.specs['name'] + '_EncPooling' + str(layer_index + 1))(x)

            if self.specs['net_params']['conv_dropouts'][layer_index] != 0.:
                x = Dropout(self.specs['net_params']['conv_dropouts'][layer_index],
                            name=self.specs['name'] + '_EncDropout' + str(layer_index + 1))(x)

        if len(self.specs['net_params']['dense_dimensions']) > 0:
            conv_output_shape = x._keras_shape[1:]
            x = Flatten(name=self.specs['name'] + '_EncFlatten')(x)
            first_last_dense_dimension = x._keras_shape[1]

            # # First dense layer, size fixed
            # if self.specs['net_params']['batch_normalization']:
            #     x = BatchNormalization()(x)
            # x = Dense(units=first_last_dense_dimension,
            #           activation=self.specs['net_params']['dense_activation'],
            #           name='EncodingDense1')(x)
            # if self.specs['net_params']['dense_dropouts'][0] != 0.:
            #     x = Dropout(rate=self.specs['net_params']['dense_dropouts'][0])(x)

            # Encoding dense layers
            for layer_index in range(len(self.specs['net_params']['dense_dimensions'])):
                if self.specs['net_params']['batch_normalization']:
                    x = BatchNormalization(name=self.specs['name'] + '_EncDenseBatchNorm' + str(layer_index + 1))(x)

                x = Dense(self.specs['net_params']['dense_dimensions'][layer_index],
                          activation=self.specs['net_params']['dense_activation'],
                          name=self.specs['name'] + '_EncDense' + str(layer_index + 1))(x)
                if self.specs['net_params']['dense_dropouts'][layer_index - 1] != 0.:
                    x = Dropout(rate=self.specs['net_params']['dense_dropouts'][layer_index],
                                name=self.specs['name'] + '_EncDenseDropout' + str(layer_index + 1))(x)

            # Decoding dense layers
            # for layer_index in reversed(range(len(self.specs['net_params']['dense_dimensions']))):
            #     if self.specs['net_params']['batch_normalization']:
            #         x = BatchNormalization(name=self.specs['name'] + '_DecDenseBatchNorm' + str(layer_index + 2))(x)
            #
            #     x = Dense(units=self.specs['net_params']['dense_dimensions'][layer_index],
            #               activation=self.specs['net_params']['dense_activation'],
            #               name=self.specs['name'] + '_DecDense' + str(layer_index + 2))(x)
            #     if self.specs['net_params']['dense_dropouts'][layer_index] != 0.:
            #         x = Dropout(rate=self.specs['net_params']['dense_dropouts'][layer_index],
            #                     name=self.specs['name'] + '_DecDenseDropout' + str(layer_index + 2))(x)

            # Last dense layer, size fixed
            if self.specs['net_params']['batch_normalization']:
                x = BatchNormalization(name=self.specs['name'] + '_DecDenseBatchNorm1')(x)

            x = Dense(units=first_last_dense_dimension,
                      activation=self.specs['net_params']['dense_activation'],
                      name=self.specs['name'] + '_DecDense1')(x)
            if self.specs['net_params']['dense_dropouts'][0] != 0.:
                x = Dropout(rate=self.specs['net_params']['dense_dropouts'][0])(x)

            x = Reshape(target_shape=conv_output_shape,
                        name=self.specs['name'] + '_DecReshape')(x)

        # Decoding convolutional layers
        for layer_index in reversed(range(len(self.specs['net_params']['conv_dimensions']) - 1)):


            # if self.specs['net_params']['pooling_windows'][layer_index] == 1:
            #     x = ZeroPadding1D(padding=(0, self.specs['net_params']['conv_windows'][layer_index] - 1))(x)
            #
            # elif layers['pool_layer'] is not None:
            #     x = layers['upsampling_layer'](size=self.specs['net_params']['pooling_windows'][layer_index],
            #                                    name=self.specs['name'] + '_DecUpsampling' + str(layer_index + 2))(x)
            #
            #     crop = get_crop_dimension(x._keras_shape[1:-1], pooling_input_dimensions.pop(-1))
            #
            #     x = layers['cropping_layer'](cropping=crop,
            #                                  name=self.specs['name'] + '_DecCropping' + str(layer_index + 2))(x)
            #
            #     x = ZeroPadding1D(padding=(0, self.specs['net_params']['conv_windows'][layer_index] - 1))(x)

            # crop = ((0,0),) * (len(x._keras_shape) - 2) if len(x._keras_shape) > 3 else (0,0)

            if layers['pool_layer'] is not None and self.specs['net_params']['pooling_windows'][layer_index] != 1:
                x = layers['upsampling_layer'](size=self.specs['net_params']['pooling_windows'][layer_index],
                                               name=self.specs['name'] + '_DecUpsampling' + str(layer_index + 2))(x)

                crop = get_crop_dimension(x._keras_shape[1:-1], pooling_input_dimensions.pop(-1))

                # to fix: crop only for input dimension == 1
                if crop[0] > 0 or crop[1] > 0:
                    x = layers['cropping_layer'](cropping=crop, name=self.specs['name'] + '_DecCropping' + str(layer_index + 2))(x)
                elif crop[0] < 0 or crop[1] < 0:
                    x = layers['zeropadding_layer'](padding=(abs(crop[0]), abs(crop[1])))(x)

            # x = layers['zeropadding_layer'](padding=(0, self.specs['net_params']['conv_windows'][layer_index] - 1))(x)
            padding = (self.specs['net_params']['conv_windows'][layer_index] - 1, self.specs['net_params']['conv_windows'][layer_index] - 1)
            x = layers['zeropadding_layer'](padding=padding)(x)

            if self.specs['net_params']['batch_normalization']:
                x = BatchNormalization(name=self.specs['name'] + '_DecBatchNorm' + str(layer_index + 2))(x)


            x = layers['conv_layer'](filters=self.specs['net_params']['conv_dimensions'][layer_index],
                                     kernel_size=self.specs['net_params']['conv_windows'][layer_index],
                                     strides=self.specs['net_params']['conv_strides'][layer_index],
                                     padding='valid',
                                     activation=self.specs['net_params']['dec_activation'],
                                     name=self.specs['name'] + '_DecConv' + str(layer_index + 2))(x)

            if self.specs['net_params']['conv_dropouts'][layer_index] != 0.:
                x = Dropout(rate=self.specs['net_params']['conv_dropouts'][layer_index],
                            name=self.specs['name'] + '_DecDropout' + str(layer_index + 2))(x)

        if layers['pool_layer'] is not None and self.specs['net_params']['pooling_windows'][0] != 1:
            x = layers['upsampling_layer'](size=self.specs['net_params']['pooling_windows'][0],
                                           name=self.specs['name'] + '_DecUpsampling1')(x)

            crop = get_crop_dimension(x._keras_shape[1:-1], pooling_input_dimensions.pop(-1))

            # to fix: crop only for input dimension == 1
            if crop[0] > 0 or crop[1] > 0:
                x = layers['cropping_layer'](cropping=crop, name=self.specs['name'] + '_DecCropping1')(x)
            elif crop[0] < 0 or crop[1] < 0:
                x = layers['zeropadding_layer'](padding=(abs(crop[0]), abs(crop[1])))(x)

        # x = layers['zeropadding_layer'](padding=(0, self.specs['net_params']['conv_windows'][0] - 1))(x)
        padding = (self.specs['net_params']['conv_windows'][0] - 1, self.specs['net_params']['conv_windows'][0] - 1)
        x = layers['zeropadding_layer'](padding=padding)(x)

        if self.specs['net_params']['batch_normalization']:
            x = BatchNormalization(name=self.specs['name'] + '_DecBatchNorm1')(x)

        if self.specs['net_params']['output_activation'] == 'relu' \
                or self.specs['net_params']['output_activation'] == 'clipped_relu':

            x = layers['conv_layer'](
                filters=self.specs['output_dims'][next(iter(self.specs['output_dims']))][-1],
                kernel_size=self.specs['net_params']['conv_windows'][0],
                strides=self.specs['net_params']['conv_strides'][0],
                padding='valid',
                activation='linear',
                name=self.specs['name'] + '_DecConv1_Linear')(x)

            if self.specs['net_params']['output_activation'] == 'relu':
                from keras.layers.advanced_activations import LeakyReLU
                net_output = LeakyReLU(name=self.specs['name'] + '_DecConv1_LeakyReLU')(x)
            else:
                from netcreator.custom_layers.keras_layers import ClippedLeakyReLU
                net_output = ClippedLeakyReLU(name=self.specs['name'] + '_DecConv1_ClippedLeakyReLU',
                                              max_value=self.specs['net_params']['clipping_threshold'])(x)

        else:
            net_output = layers['conv_layer'](
                filters=self.specs['output_dims'][next(iter(self.specs['output_dims']))][-1],
                kernel_size=self.specs['net_params']['conv_windows'][0],
                strides=self.specs['net_params']['conv_strides'][0],
                padding='valid',
                activation=self.specs['net_params']['output_activation'],
                name=self.specs['name'] + '_DecConv1')(x)

        model = Model(inputs=input_layer, outputs=net_output)

        model.summary()

        self.model = model


class KerasMultiOutConvAutoEncoder(_ConvAutoEncoder, _KerasConvNet):
    def build_model(self):
        from keras.models import Model
        from keras.layers import Flatten, Reshape, Dropout, Dense, BatchNormalization, GaussianNoise
        from ..utils.net_utils import get_crop_dimension

        pooling_input_dimensions = []

        input_layers = self.get_input_layers()
        input_layer = input_layers[next(iter(input_layers))]

        layers = self.get_conv_layers()

        # Network instantiation
        x = input_layer

        if self.specs['net_params']['input_gaussian_noise'] != 0.:
            x = GaussianNoise(stddev=self.specs['net_params']['input_gaussian_noise'],
                              name=self.specs['name'] + '_GaussianNoise')(x)
        # Encoding convolutional layers
        for layer_index in range(len(self.specs['net_params']['conv_dimensions'])):

            if layer_index > 0 and self.specs['net_params']['batch_normalization']:
                x = BatchNormalization(name=self.specs['name'] + '_EncConvBatchNorm' + str(layer_index + 1))(x)
            x = layers['conv_layer'](filters=self.specs['net_params']['conv_dimensions'][layer_index],
                                     kernel_size=self.specs['net_params']['conv_windows'][layer_index],
                                     strides=self.specs['net_params']['conv_strides'][layer_index],
                                     padding='valid',
                                     activation=self.specs['net_params']['enc_activation'],
                                     name=self.specs['name'] + '_EncConv' + str(layer_index + 1))(x)

            pooling_input_dimensions.append(x._keras_shape[1:-1])

            if layers['pool_layer'] is not None:
                x = layers['pool_layer'](pool_size=self.specs['net_params']['pooling_windows'][layer_index],
                                         strides=self.specs['net_params']['pooling_strides'][layer_index],
                                         padding='valid',
                                         name=self.specs['name'] + '_EncPooling' + str(layer_index + 1))(x)

            if self.specs['net_params']['conv_dropouts'][layer_index] != 0.:
                x = Dropout(self.specs['net_params']['conv_dropouts'][layer_index],
                            name=self.specs['name'] + '_EncDropout' + str(layer_index + 1))(x)

        if len(self.specs['net_params']['dense_dimensions']) > 0:
            conv_output_shape = x._keras_shape[1:]
            x = Flatten(name=self.specs['name'] + '_EncFlatten')(x)
            first_last_dense_dimension = x._keras_shape[1]

            # # First dense layer, size fixed
            # if self.specs['net_params']['batch_normalization']:
            #     x = BatchNormalization()(x)
            # x = Dense(units=first_last_dense_dimension,
            #           activation=self.specs['net_params']['dense_activation'],
            #           name='EncodingDense1')(x)
            # if self.specs['net_params']['dense_dropouts'][0] != 0.:
            #     x = Dropout(rate=self.specs['net_params']['dense_dropouts'][0])(x)

            # Encoding dense layers
            for layer_index in range(len(self.specs['net_params']['dense_dimensions'])):
                if self.specs['net_params']['batch_normalization']:
                    x = BatchNormalization(name=self.specs['name'] + '_EncDenseBatchNorm' + str(layer_index + 1))(x)

                x = Dense(self.specs['net_params']['dense_dimensions'][layer_index],
                          activation=self.specs['net_params']['dense_activation'],
                          name=self.specs['name'] + '_EncDense' + str(layer_index + 1))(x)
                if self.specs['net_params']['dense_dropouts'][layer_index - 1] != 0.:
                    x = Dropout(rate=self.specs['net_params']['dense_dropouts'][layer_index],
                                name=self.specs['name'] + '_EncDenseDropout' + str(layer_index + 1))(x)

            # Decoding dense layers
            # for layer_index in reversed(range(len(self.specs['net_params']['dense_dimensions']))):
            #     if self.specs['net_params']['batch_normalization']:
            #         x = BatchNormalization(name=self.specs['name'] + '_DecDenseBatchNorm' + str(layer_index + 2))(x)
            #
            #     x = Dense(units=self.specs['net_params']['dense_dimensions'][layer_index],
            #               activation=self.specs['net_params']['dense_activation'],
            #               name=self.specs['name'] + '_DecDense' + str(layer_index + 2))(x)
            #     if self.specs['net_params']['dense_dropouts'][layer_index] != 0.:
            #         x = Dropout(rate=self.specs['net_params']['dense_dropouts'][layer_index],
            #                     name=self.specs['name'] + '_DecDenseDropout' + str(layer_index + 2))(x)

            # Last dense layer, size fixed
            if self.specs['net_params']['batch_normalization']:
                x = BatchNormalization(name=self.specs['name'] + '_DecDenseBatchNorm1')(x)

            x = Dense(units=first_last_dense_dimension,
                      activation=self.specs['net_params']['dense_activation'],
                      name=self.specs['name'] + '_DecDense1')(x)
            if self.specs['net_params']['dense_dropouts'][0] != 0.:
                x = Dropout(rate=self.specs['net_params']['dense_dropouts'][0])(x)

            x = Reshape(target_shape=conv_output_shape,
                        name=self.specs['name'] + '_DecReshape')(x)

        # Decoding convolutional layers
        for layer_index in reversed(range(len(self.specs['net_params']['conv_dimensions']) - 1)):

            if layers['pool_layer'] is not None:
                x = layers['upsampling_layer'](size=self.specs['net_params']['pooling_windows'][layer_index],
                                               name=self.specs['name'] + '_DecUpsampling' + str(layer_index + 2))(x)

                crop = get_crop_dimension(x._keras_shape[1:-1], pooling_input_dimensions.pop(-1))

                x = layers['cropping_layer'](cropping=crop,
                                             name=self.specs['name'] + '_DecCropping' + str(layer_index + 2))(x)

            if self.specs['net_params']['batch_normalization']:
                x = BatchNormalization(name=self.specs['name'] + '_DecBatchNorm' + str(layer_index + 2))(x)

            x = layers['conv_layer'](filters=self.specs['net_params']['conv_dimensions'][layer_index],
                                     kernel_size=self.specs['net_params']['conv_windows'][layer_index],
                                     strides=self.specs['net_params']['conv_strides'][layer_index],
                                     padding='same',
                                     activation=self.specs['net_params']['dec_activation'],
                                     name=self.specs['name'] + '_DecConv' + str(layer_index + 2))(x)

            if self.specs['net_params']['conv_dropouts'][layer_index] != 0.:
                x = Dropout(rate=self.specs['net_params']['conv_dropouts'][layer_index],
                            name=self.specs['name'] + '_DecDropout' + str(layer_index + 2))(x)

        if layers['pool_layer'] is not None:
            x = layers['upsampling_layer'](size=self.specs['net_params']['pooling_windows'][0],
                                           name=self.specs['name'] + '_DecUpsampling1')(x)

            crop = get_crop_dimension(x._keras_shape[1:-1], pooling_input_dimensions.pop(-1))

            x = layers['cropping_layer'](cropping=crop, name=self.specs['name'] + '_DecCropping1')(x)

        if self.specs['net_params']['batch_normalization']:
            x = BatchNormalization(name=self.specs['name'] + '_DecBatchNorm1')(x)

        last_common_layer = x
        net_output_list = []

        for output in range(self.specs['output_dims'][next(iter(self.specs['output_dims']))][-1]):

            if self.specs['net_params']['output_activation'] == 'relu' \
                    or self.specs['net_params']['output_activation'] == 'clipped_relu':

                x = layers['conv_layer'](
                    filters=1,
                    kernel_size=self.specs['net_params']['conv_windows'][0],
                    strides=self.specs['net_params']['conv_strides'][0],
                    padding='same',
                    activation='linear',
                    name='{}_output_{}_DecConv1_Linear'.format(self.specs['name'], output))(last_common_layer)

                if self.specs['net_params']['output_activation'] == 'relu':
                    from keras.layers.advanced_activations import LeakyReLU
                    net_output = LeakyReLU(
                        name='{}_output_{}_DecConv1_LeakyReLU'.format(self.specs['name'], output))(x)
                else:
                    from netcreator.custom_layers.keras_layers import ClippedLeakyReLU
                    net_output = ClippedLeakyReLU(
                        name='{}_output_{}_DecConv1_ClippedLeakyReLU'.format(self.specs['name'], output),
                        max_value=self.specs['net_params']['clipping_threshold'])(x)

            else:
                net_output = layers['conv_layer'](
                    filters=1,
                    kernel_size=self.specs['net_params']['conv_windows'][0],
                    strides=self.specs['net_params']['conv_strides'][0],
                    padding='same',
                    activation=self.specs['net_params']['output_activation'],
                    name='{}_output_{}_DecConv1'.format(self.specs['name'], output))(last_common_layer)

            net_output_list.append(net_output)

        model = Model(inputs=input_layer, outputs=net_output_list)

        model.summary()

        self.model = model


class KerasMLPAutoEncoder(_MLPAutoEncoder, _KerasNet):
    def build_model(self):
        from keras.models import Model
        from keras.layers import Flatten, Reshape, Dropout, Dense, BatchNormalization, GaussianNoise

        input_layers = self.get_input_layers()
        input_layer = input_layers[next(iter(input_layers))]

        x = input_layer

        if self.specs['net_params']['input_gaussian_noise'] != 0.:
            x = GaussianNoise(stddev=self.specs['net_params']['input_gaussian_noise'],
                              name=self.specs['name'] + '_GaussianNoise')(x)

        # Encoding dense layers
        for layer_index in range(len(self.specs['net_params']['dense_dimensions'])):
            if self.specs['net_params']['batch_normalization']:
                x = BatchNormalization(name=self.specs['name'] + '_EncDenseBatchNorm' + str(layer_index + 1))(x)

            x = Dense(self.specs['net_params']['dense_dimensions'][layer_index],
                      activation=self.specs['net_params']['enc_activation'],
                      name=self.specs['name'] + '_EncDense' + str(layer_index + 1))(x)
            if self.specs['net_params']['dense_dropouts'][layer_index - 1] != 0.:
                x = Dropout(rate=self.specs['net_params']['dense_dropouts'][layer_index],
                            name=self.specs['name'] + '_EncDenseDropout' + str(layer_index + 1))(x)

        # Decoding dense layers
        for layer_index in reversed(range(len(self.specs['net_params']['dense_dimensions']))):
            if self.specs['net_params']['batch_normalization']:
                x = BatchNormalization(name=self.specs['name'] + '_DecDenseBatchNorm' + str(layer_index + 1))(x)

            x = Dense(units=self.specs['net_params']['dense_dimensions'][layer_index],
                      activation=self.specs['net_params']['dec_activation'],
                      name=self.specs['name'] + '_DecDense' + str(layer_index + 1))(x)
            if self.specs['net_params']['dense_dropouts'][layer_index] != 0.:
                x = Dropout(rate=self.specs['net_params']['dense_dropouts'][layer_index],
                            name=self.specs['name'] + '_DecDenseDropout' + str(layer_index + 1))(x)

        if self.specs['net_params']['output_activation'] == 'relu' \
                or self.specs['net_params']['output_activation'] == 'clipped_relu':

            x = Dense(
                self.specs['output_dims'][next(iter(self.specs['output_dims']))][-1],
                activation='linear',
                name=self.specs['name'] + '_Output_Dense_Linear')(x)

            if self.specs['net_params']['output_activation'] == 'relu':
                from keras.layers.advanced_activations import LeakyReLU
                net_output = LeakyReLU(name=self.specs['name'] + '_Output_Dense_LeakyReLU')(x)
            else:
                from netcreator.custom_layers.keras_layers import ClippedLeakyReLU
                net_output = ClippedLeakyReLU(name=self.specs['name'] + '_Output_Dense_ClippedLeakyReLU',
                                              max_value=self.specs['net_params']['clipping_threshold'])(x)

        else:
            net_output = Dense(
                self.specs['output_dims'][next(iter(self.specs['output_dims']))][-1],
                activation=self.specs['net_params']['output_activation'],
                name=self.specs['name'] + '_Output_Dense')(x)

        model = Model(inputs=input_layer, outputs=net_output)

        model.summary()

        self.model = model
