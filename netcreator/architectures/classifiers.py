from __future__ import absolute_import

from ..base.convolutionals import _ConvNet, _KerasConvNet


class _ConvClassifier(_ConvNet):
    def get_default_parameters(self):

        net_params = {
            'conv_dimensions': [8],
            'conv_activation': 'relu',
            'conv_windows': [5],
            'conv_strides': [1],
            'conv_dropouts': [0.],
            'pooling_type': 'max',
            'pooling_windows': [2],
            'padding_type': 'same',
            'dense_dimensions': [128],
            'dense_activation': 'relu',
            'dense_dropouts': [0.],
            'batch_normalization': False,
            'output_function': 'softmax'
        }

        return net_params

    def check_parameters(self, net_params):
        n_conv = len(net_params['conv_dimensions'])
        n_dense = len(net_params['dense_dimensions'])

        if n_conv < 1:
            raise ValueError('You need at least one convolutional layer.')

        if isinstance(net_params['conv_dropouts'], float):
            net_params['conv_dropouts'] = [net_params['conv_dropouts']]
            for i in range(n_conv - 1):
                net_params['conv_dropouts'].append(net_params['conv_dropouts'][0])

        if isinstance(net_params['conv_dropouts'], list):
            if n_conv != len(net_params['conv_dropouts']):
                raise ValueError('Convolutional layer dimensions and dropouts not matching.')

        if n_conv != len(net_params['conv_windows']):
            raise ValueError('Convolutional layer dimensions and windows not matching.')

        if n_conv != len(net_params['conv_strides']):
            raise ValueError('Convolutional layer dimensions and strides not matching.')

        if n_conv != len(net_params['pooling_windows']):
            raise ValueError('Convolutional layer dimensions and pooling windows not matching.')

        if isinstance(net_params['dense_dropouts'], float):
            net_params['dense_dropouts'] = [net_params['dense_dropouts']]
            for i in range(n_dense - 1):
                net_params['dense_dropouts'].append(net_params['dense_dropouts'][0])

        if isinstance(net_params['dense_dropouts'], list):
            if n_conv != len(net_params['dense_dropouts']):
                raise ValueError('Dense layer dimensions and dropouts not matching.')

        if net_params['pooling_type'] != 'max' and net_params['pooling_type'] != 'average':
            raise ValueError('Only max or average pooling supported.')

        if net_params['batch_normalization']:
            if not all(conv_dropout == 0. for conv_dropout in net_params['conv_dropouts']):
                raise ValueError('Use either dropout or batch normalization.')
            elif not all(dense_dropout == 0. for dense_dropout in net_params['dense_dropouts']):
                raise ValueError('Use either dropout or batch normalization.')


class KerasConvClassifier(_ConvClassifier, _KerasConvNet):
    def build_model(self):
        from keras.models import Model
        from keras.layers import Flatten, Dropout, Dense, BatchNormalization

        input_layers = self.get_input_layers()
        input_layer = input_layers[next(iter(input_layers))]

        layers = self.get_conv_layers()

        # Network instantiation
        x = input_layer

        # Encoding convolutional layers
        for layer_index in range(len(self.specs['net_params']['conv_dimensions'])):

            if layer_index > 0 and self.specs['net_params']['batch_normalization']:
                x = BatchNormalization(name=self.specs['name'] + '_ConvBatchNorm' + str(layer_index + 1))(x)
            x = layers['conv_layer'](self.specs['net_params']['conv_dimensions'][layer_index],
                                     self.specs['net_params']['conv_windows'][layer_index],
                                     strides=self.specs['net_params']['conv_strides'][layer_index],
                                     padding=self.specs['net_params']['padding_type'],
                                     activation=self.specs['net_params']['conv_activation'],
                                     name=self.specs['name'] + '_Conv' + str(layer_index + 1))(x)

            x = layers['pool_layer'](pool_size=self.specs['net_params']['pooling_windows'][layer_index],
                                     name = self.specs['name'] + '_Pooling' + str(layer_index + 1))(x)

            if self.specs['net_params']['conv_dropouts'][layer_index] != 0.:
                x = Dropout(self.specs['net_params']['conv_dropouts'][layer_index],
                            name=self.specs['name'] + 'ConvDropout' + str(layer_index + 1))(x)

        x = Flatten(name=self.specs['name'] + '_Flatten')(x)

        if len(self.specs['net_params']['dense_dimensions']) > 0:
            for layer_index in range(len(self.specs['net_params']['dense_dimensions'])):
                if self.specs['net_params']['batch_normalization']:
                    x = BatchNormalization(name=self.specs['name'] + '_DenseBatchNorm' + str(layer_index + 1))(x)

                x = Dense(self.specs['net_params']['dense_dimensions'][layer_index],
                          activation=self.specs['net_params']['dense_activation'],
                          name=self.specs['name'] + '_Dense' + str(layer_index + 1))(x)
                if self.specs['net_params']['dense_dropouts'][layer_index] != 0.:
                    x = Dropout(self.specs['net_params']['dense_dropouts'][layer_index],
                                name=self.specs['name'] + '_DenseDropout' + str(layer_index + 1))(x)

        net_output = Dense(self.specs['output_dims'][0], activation=self.specs['net_params']['output_function'],
                           name=self.specs['name'] + '_OutputDense')(x)

        model = Model(inputs=input_layer, outputs=net_output)

        model.summary()

        self.model = model
