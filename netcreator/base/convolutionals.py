from ..base.networks import _Net, _KerasNet


class _ConvNet(_Net):
    def get_convolution_dimension(self):
        # Check on the input dimension to get the convolution dimension
        if not self.specs['input_dims']:
            raise ValueError('Input dimension is needed to setup the default parameters.')
        elif len(self.specs['input_dims']) > 1:
            raise ValueError('This architecture supports only a single input')
        elif len(self.specs['input_dims'][next(iter(self.specs['input_dims']))]) == 3:
            dimension = 1
        elif len(self.specs['input_dims'][next(iter(self.specs['input_dims']))]) == 4:
            dimension = 2
        else:
            raise ValueError('Something went wrong with your input dimensions {}.'.format(self.specs['input_dims']))

        return dimension


class _KerasConvNet(_ConvNet, _KerasNet):
    def get_conv_layers(self):

        dimension = self.get_convolution_dimension()

        if dimension == 1:
            from keras.layers import Conv1D, MaxPool1D, AveragePooling1D, UpSampling1D, Cropping1D, ZeroPadding1D

            conv_layer = Conv1D

            if self.specs['net_params']['pooling_type'] == 'average':
                pool_layer = AveragePooling1D
            elif self.specs['net_params']['pooling_type'] == 'max':
                pool_layer = MaxPool1D
            elif self.specs['net_params']['pooling_type'] == 'none':
                pool_layer = None
            else:
                raise ValueError('Pooling type {} not supported.'.format(self.specs['net_params']['pooling_type']))

            upsampling_layer = UpSampling1D

            cropping_layer = Cropping1D

            zeropadding_layer = ZeroPadding1D

        elif dimension == 2:
            from keras.layers import Conv2D, MaxPool2D, AveragePooling2D, UpSampling2D, Cropping2D, ZeroPadding2D

            conv_layer = Conv2D

            if self.specs['net_params']['pooling_type'] == 'average':
                pool_layer = AveragePooling2D
            elif self.specs['net_params']['pooling_type'] == 'max':
                pool_layer = MaxPool2D
            elif self.specs['net_params']['pooling_type'] == 'none':
                pool_layer = None
            else:
                raise ValueError('Pooling type {} not supported.'.format(self.specs['net_params']['pooling_type']))

            upsampling_layer = UpSampling2D

            cropping_layer = Cropping2D

            zeropadding_layer = ZeroPadding2D

        else:
            raise ValueError('Dimension {} not supported.'.format(dimension))

        return {'conv_layer': conv_layer,
                'pool_layer': pool_layer,
                'upsampling_layer': upsampling_layer,
                'cropping_layer': cropping_layer,
                'zeropadding_layer': zeropadding_layer}
