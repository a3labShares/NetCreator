from __future__ import absolute_import

import os

from netcreator.utils import io_utils


class _Net(object):
    def __init__(self, name=None, input_dims=None, output_dims=None, input_params=None):
        self.specs = dict()
        self._specs_keys = ['name', 'input_dims', 'output_dims', 'net_params']

        self.model = None

        if input_dims is None:
            print('Initializing dummy network, make sure you load or initialize it.')
        else:
            print('Initializing network.')
            self.init_network(input_dims=input_dims, output_dims=output_dims, input_params=input_params, name=name)

    def init_network(self, input_dims, output_dims=None, input_params=None, name=None):
        self.specs = dict()

        input_dims = io_utils.dimension_check(dims=input_dims)
        self.specs.update({'input_dims': input_dims})

        if output_dims is not None:
            output_dims = io_utils.dimension_check(dims=output_dims)
            self.specs.update({'output_dims': output_dims})
        else:
            print('No output dimension given, setting it equal to the input dimension.')
            self.specs.update({'output_dims': input_dims})

        self.init_parameters(input_params=input_params)

        if name is None or not isinstance(name, str):
            self.specs.update({'name': 'noname'})
        else:
            self.specs.update({'name': name})

        self.build_model()

    def _set_specs(self, input_specs):
        required_keys = [k for k in self._specs_keys]
        for i, k in enumerate(self._specs_keys):
            try:
                self.specs.update({k: input_specs[k]})
                required_keys.remove(k)
            except (TypeError, ValueError) as err:
                return "Error: {}".format(err)
        if len(required_keys) > 0:
            raise IOError('Not all required keys were given, missing {}.'.format(required_keys))

    def save_specs(self, net_path, filename='specs.json', overwrite=False):
        io_utils.save_json(path=net_path, filename=filename, dictionary=self.specs, overwrite=overwrite)

    def load_specs(self, net_path, spec_filename='specs.json'):
        specs = io_utils.load_json(net_path, spec_filename)
        self._set_specs(specs)

    def get_spec(self, spec):
        if spec in self._specs_keys:
            return self.specs[spec]
        else:
            print("WARNING: found invalid spec {}.".format(spec))

    def init_parameters(self, input_params=None):

        net_params = self.get_default_parameters()

        if input_params is None:
            print('Setting default parameter configuration.')

        elif isinstance(input_params, dict):
            for k in input_params.keys():
                if k not in net_params.keys():
                    print('WARNING: ignoring unrecognized argument {}.'.format(k))
                else:
                    print('Setting {} to {}'.format(k, input_params[k]))
                    net_params[k] = input_params[k]

        self.check_parameters(net_params)
        self.specs.update({'net_params': net_params})

    def get_default_parameters(self):
        # To be implemented for each different architecture (e.g. AutoEncoder).
        raise NotImplemented

    def check_parameters(self, net_params):
        # To be implemented for each different architecture (e.g. AutoEncoder).
        raise NotImplemented

    def build_model(self):
        # To be implemented for each different builder (e.g. keras)
        raise NotImplemented

    def save_activations(self, net_path, filename='activations.h5', overwrite=False):
        raise NotImplemented

    def save(self, net_path, spec_filename='specs.json', activation_filename='activations.h5', overwrite=False):
        print('Saving network to {}...'.format(net_path))
        self.save_specs(net_path=net_path, filename=spec_filename, overwrite=overwrite)
        self.save_activations(net_path=net_path, filename=activation_filename, overwrite=overwrite)

    def load_activations(self, net_path, filename='activations.h5'):
        raise NotImplemented

    def load(self, net_path, spec_filename='specs.json', activation_filename='activations.h5'):
        print('Loading network from {}...'.format(net_path))
        self.load_specs(net_path, spec_filename)
        self.build_model()
        self.load_activations(net_path, activation_filename)

    def compile(self, optimizer, loss, **kwargs):
        raise NotImplemented

    def validation_check(self, x_valid, y_valid):
        raise NotImplemented

    def fit(self):
        raise NotImplemented

    def predict(self, x):
        raise NotImplemented

    def evaluate(self, x_eval, y_eval, **kwargs):
        # Returns loss for given
        raise NotImplemented


class _KerasNet(_Net):
    def get_input_layers(self):
        from keras.layers import Input

        input_layers = {}

        for input_name, input_dim in self.specs['input_dims'].items():

            layer_name = '{}_{}_input'.format(self.specs['name'], input_name)

            if len(input_dim) == 2:
                input_layer = Input((self.specs['input_dims'][input_name][1], ),
                                    name=layer_name)

            elif len(input_dim) == 3:
                input_layer = Input((self.specs['input_dims'][input_name][1],
                                     self.specs['input_dims'][input_name][2]),
                                    name=layer_name)

            elif len(input_dim) == 4:
                input_layer = Input((self.specs['input_dims'][input_name][1],
                                     self.specs['input_dims'][input_name][2],
                                     self.specs['input_dims'][input_name][3]),
                                    name=layer_name)

            else:
                raise ValueError('Input dimension {} not supported.'.format(input_dim))

            input_layers.update({input_name: input_layer})

        return input_layers

    def save_activations(self, net_path, filename='activations.h5', overwrite=False):
        if os.path.splitext(filename)[-1] != '.h5':
            raise ValueError('Wrong extension given for filename {}, use h5 instead.'.format(filename))
        self.model.save_weights(os.path.join(net_path, filename))

    def load_activations(self, net_path, filename='activations.h5'):
        self.model.load_weights(os.path.join(net_path, filename))

    def compile(self, optimizer, loss, metrics=None, loss_weights=None, sample_weight_mode=None, **kwargs):
        print('Compiling model...')
        self.model.compile(optimizer, loss, metrics=None, loss_weights=None, sample_weight_mode=None, **kwargs)
        print('Model compiled.')

    def fit(self, x=None, y=None, batch_size=32, epochs=1, validation_split=0.0, validation_data=None, **kwargs):
        return self.model.fit(x=x, y=y,
                              batch_size=batch_size,
                              epochs=epochs,
                              validation_split=validation_split,
                              validation_data=validation_data, **kwargs)

    def train_on_batch(self, x, y, class_weight=None, sample_weight=None):
        return self.model.train_on_batch(x, y, class_weight=class_weight, sample_weight=sample_weight)

    def evaluate(self, x_eval, y_eval, **kwargs):
        return self.model.evaluate(x_eval, y_eval)

    def predict(self, x):
        return self.model.predict(x)



