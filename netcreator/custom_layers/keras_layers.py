# import theano

from keras.layers import Layer
from keras import backend as K


class ClippedLeakyReLU(Layer):

    def __init__(self, alpha=0.3, max_value=1000., **kwargs):
        self.supports_masking = True
        self.alpha = K.cast_to_floatx(alpha)
        self.max_value = K.cast_to_floatx(max_value)
        super(ClippedLeakyReLU, self).__init__(**kwargs)

    def call(self, inputs, **kwargs):
        return K.relu(inputs, alpha=self.alpha, max_value=self.max_value)

    def get_config(self):
        config = {'alpha': float(self.alpha)}
        base_config = super(ClippedLeakyReLU, self).get_config()
        return dict(list(base_config.items()) + list(config.items()))


# class InverseLayer(Layer):
#     def __init__(self, tensor_to_invert, input_to_tensor_to_invert, **kwargs):
#         self.tensor_to_invert = tensor_to_invert
#         self.input_to_tensor_to_invert = input_to_tensor_to_invert
#         super(InverseLayer, self).__init__(**kwargs)
#
#     def call(self, inputs, **kwargs):
#         return theano.grad(None, wrt=self.input_to_tensor_to_invert, known_grads={self.tensor_to_invert: inputs})
